# Notify Cohorts #

This plugin allows to send notifications to the members of selected cohorts.
You just need to go to "Site adminsitration / Users / Notify cohorts", type the subsject and the body you want for the notification and select which cohorts will be notified.
The notification will be sent as "system notification" on Moodle, not as a message from the user.
Only users with 'moodle/site:config' capability permitted can send notifications.

## License ##

2021 Daniel Neis Araujo <daniel@adapta.online>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
