<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     local_notifycohorts
 * @category    string
 * @copyright   2021 Daniel Neis Araujo <daniel@adapta.online>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['availablecohortsnotification'] = 'Select which cohorts you would like to send notification to members:';
$string['body'] = 'Body';
$string['bodyrequired'] = 'You must provide a body';
$string['cohorts'] = 'Destination cohorts';
$string['nocohorts'] = 'No cohorts available. You need to create at least one cohort to use this plugin.';
$string['notification'] = 'Send notifications';
$string['notificationssent'] = 'Notifications sent.';
$string['pluginname'] = 'Notify Cohorts';
$string['privacy:metadata'] = 'This plugin does not store any user data.';
$string['subject'] = 'Subject';
$string['subjectrequired'] = 'You must provide a subject';
